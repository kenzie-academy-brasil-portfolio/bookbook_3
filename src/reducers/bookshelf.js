const defaultState = {
  //  cReading = currently reading, aRead = alreadyRead, toRead = toReadList
  books: [],
};

const bookshelf = (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_BOOKS':
      const { books } = action;
      return { ...state, books };
    default:
      return state;
  }
};

export default bookshelf;

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Container, Button } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { OutlinedInput } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";

const styles = () => ({
  background: {
    backgroundColor: "GhostWhite",
    // height: "1000px",
  },
  title: {
    color: "#3f51b5",
    fontFamily: "Monda",
    textTransform: "uppercase",
    fontSize: "16px",
    marginTop: "10px",
    marginBottom: "0px",
    textAlign: "center",
  },
  superior: {
    width: "100%",
    height: "4px",
    backgroundColor: "rgba(18,29,77,1)",
    borderRadius: "5px",
  },
  author: {
    color: "#374242",
    fontFamily: "Monda",
    fontSize: "15px",
    marginTop: "0px",
    marginBottom: "0px",
    margin: "0 auto",
    textAlign: "center",
  },
  item: {
    margin: "0 auto",
    width: "100%",
    minWidth: "300px",
    maxWidth: "300px",
    minHeight: "550px",
  },
  buscar: {
    backgroundColor: "black",
    width: "100%",
    margin: "0 auto",
    color: "white",
    fontFamily: "Monda",
    fontWeight: "900",
    fontSize: "32px",
    textAlign: "center",
    marginBottom: "50px",
  },
  searchBox: {
    marginTop: "10px",
    margin: "0 auto",
    textAlign: "center",
  },
  textField: {
    borderRadius: "50px",
    width: "25%",
    textAlign: "center",
    backgroundColor: "white",
  },
  cards: {
    marginLeft: "8px",
    marginRight: "8px",
  },
  searchButton: {
    background: "linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1) 100%)",
    color: "ghostwhite",
    borderColor: "ghostwhite",

    borderRadius: "50px",
  },
  position: {
    width: "1250px",
    marginTop: "54px",
    fontFamily: "Monda",
    fontSize: "16px",
    margin: "0 auto",
  },
  verticalSpace: {
    backgroundColor: "GhostWhite	",
  },
  button: {
    backgroundColor: "#antiquewhite",
    color: "darkslateblue",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "darkslateblue",
  },
  button2: {
    backgroundColor: "#antiquewhite",
    color: "crimson",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "crimson",
  },
  bookbook2: {
    margin: "0 auto",
    height: "100px",
    width: "940px",
    backgroundColor: "black",
    position: "fixed",
    top: "385px",
    left: "-430px",
    fontSize: "32px",
    textAlign: "center",
    fontWeight: "900",
    color: "ghostwhite",
    fontFamily: "Monda",
    transform: "rotate(270deg)",
    lineHeight: "100px",
  },
});

class Search extends React.Component {
  state = {
    books: [],
    searchBooks: "",
  };

  handleSearch = (e) => {
    this.setState({ searchBooks: e.target.value });
  };

  searchBooks = (e) => {
    e.preventDefault();

    const q = this.state.searchBooks;
    const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=${q}`;
    if (this.state.searchBooks === "" || this.state.searchBooks === " ") {
      alert("Insira algo no campo de busca!");
    } else {
      fetch(apiUrl)
        .then((res) => res.json())
        .then((res) => {
          console.log(res.items);
          this.setState({ books: res.items, searchBooks: "" });
        });
    }
  };

  moveSearchToWishList = (book, imageDefault) => {
    return fetch(
      `https://ka-users-api.herokuapp.com/users/${this.props.user.id}/books`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.currentToken,
        },
        body: JSON.stringify({
          book: {
            title: book.volumeInfo.title,
            author: book.volumeInfo.authors[0],
            shelf: 1,
            image_url: (
              book.volumeInfo.imageLinks
                ? book.volumeInfo.imageLinks.smallThumbnail
                : imageDefault),
            grade: 1,
            review: "",
            google_book_id: book.id,
          },
        }),
      }
    ).then(alert("Livro adicionado em 'Desejos de leitura'!"));
  };

  moveSearchToReading = (book, imageDefault) => {
    return fetch(
      `https://ka-users-api.herokuapp.com/users/${this.props.user.id}/books`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.currentToken,
        },
        body: JSON.stringify({
          book: {
            title: book.volumeInfo.title,
            author: book.volumeInfo.authors[0],
            shelf: 2,
            image_url: (
              book.volumeInfo.imageLinks
                ? book.volumeInfo.imageLinks.smallThumbnail
                : imageDefault),
            grade: 1,
            review: "",
            google_book_id: book.id,
          },
        }),
      }
    ).then(alert("Livro adicionado em 'Em leitura'!"));
  };

  moveSearchToRead = (book, imageDefault) => {
    return fetch(
      `https://ka-users-api.herokuapp.com/users/${this.props.user.id}/books`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.currentToken,
        },
        body: JSON.stringify({
          book: {
            title: book.volumeInfo.title,
            author: book.volumeInfo.authors[0],
            shelf: 3,
            image_url: (
              book.volumeInfo.imageLinks
                ? book.volumeInfo.imageLinks.smallThumbnail
                : imageDefault),
            grade: 1,
            review: "",
            google_book_id: book.id,
          },
        }),
      }
    ).then(alert("Livro adicionado em 'Lidos'!"));
  };

  render() {
    console.log(this.state.books);
    console.log(this.state);
    const { classes } = this.props;
    const books = this.state.books;
    const imageDefault =
      "https://s3-us-west-2.amazonaws.com/s.cdpn.io/387928/book%20placeholder.png";

    return (
      <div className={classes.background}>
        <div className={classes.background}>
          <div className={classes.buscar}>Buscar</div>
          <div className={classes.bookbook2}>BookBook</div>
          <form onSubmit={this.searchBooks}>
            <Container className={classes.searchBox}>
              <OutlinedInput
                className={classes.textField}
                margin="dense"
                onChange={this.handleSearch}
                value={this.state.searchBooks}
              />
            </Container>
          </form>
          <Container className={classes.searchBox}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              onClick={this.searchBooks}
              className={classes.searchButton}
            >
              <SearchIcon />
            </Button>
          </Container>
          <Container justify="center" className={classes.position}>
            <Grid
              direction="Row"
              wrap="wrap"
              className={classes.verticalSpace}
              container="true"
              alignContent="space-around"
              alignItems="stretch"
              spacing={10}
            >
              {books.map((book, key) => (
                <>
                  <Grid className={classes.cards}>
                    <Paper elevation={24} className={classes.item} key={key}>
                      <div div className={classes.superior}>
                        <br />
                        <Container>
                          <br />
                          <h1 className={classes.title}>
                            {book.volumeInfo.title}
                          </h1>
                          <h2 className={classes.author}>
                            {book.volumeInfo.authors}
                          </h2>
                          <hr />
                          <div
                            style={{
                              width: 128,
                              height: 190,
                              marginLeft: 60,
                              marginTop: 10,
                              backgroundImage: `url("${
                                book.volumeInfo.imageLinks
                                  ? book.volumeInfo.imageLinks.smallThumbnail
                                  : imageDefault
                                }")`,
                              backgroundSize: "128px 190px",
                            }}
                          />
                          <hr />
                          <br />

                          <br />
                          <Button
                            className={classes.button}
                            size="small"
                            variant="outlined"
                            color="primary"
                            onClick={() => this.moveSearchToWishList(book, imageDefault)}
                          >
                            Quero Ler
                          </Button>
                          <Button
                            className={classes.button}
                            size="small"
                            variant="outlined"
                            color="primary"
                            onClick={() => this.moveSearchToReading(book, imageDefault)}
                          >
                            Lendo
                          </Button>
                          <Button
                            className={classes.button2}
                            size="small"
                            variant="outlined"
                            color="secondy"
                            onClick={() => this.moveSearchToRead(book, imageDefault)}
                          >
                            Lido
                          </Button>
                          <br />
                          <br />
                        </Container>
                      </div>
                    </Paper>
                    <br />
                    <br />
                  </Grid>
                </>
              ))}
            </Grid>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

export default connect(mapStateToProps)(withStyles(styles)(Search));

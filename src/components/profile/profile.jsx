import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Container } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import ReadBooks from "../ReadBooks/ReadBooks";
import UserPerfil from "../userPerfil/UserPerfil";
import WishCard from "../WishList/WishCard";
import ReadingBooks from "../ReadingBooks/ReadingBooks";
import loadBooksList from "../helpers/loadBooksList";
import { addBooks } from "../../action";
const useStyles = (theme) => ({
  position: {
    width: "1200px",
    marginTop: "-130px",
  },
  background: {
    backgroundColor: "GhostWhite	",
  },
  paper: {
    padding: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "transparent",
    textAlign: "center",
    color: theme.palette.text.secondary,
    zoom: "0.95",
  },
  bookbook2: {
    backgroundColor: "black",
    width: "100%",
    margin: "0 auto",
    color: "black",
    fontFamily: "Monda",
    fontWeight: "900",
    fontSize: "32px",
    textAlign: "center",

    marginBottom: "100px",
  },
  bookbook: {
    margin: "0 auto",
    height: "100px",
    width: "940px",
    backgroundColor: "black",
    position: "fixed",
    top: "385px",
    left: "-430px",
    fontSize: "32px",
    textAlign: "center",
    fontWeight: "900",
    color: "ghostwhite",
    fontFamily: "Monda",
    transform: "rotate(270deg)",
    lineHeight: "100px",
  },
});

class Profile extends React.Component {
  componentDidMount() {
    loadBooksList(this.props.user.id, this.props.currentToken)
      .then((res) => res.json())
      .then((res) => {
        this.props.addBooks(res);
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        <div div className={classes.background}>
          <div className={classes.bookbook2}>Perfil</div>
          <div className={classes.bookbook}>BookBook</div>
          <Container justify="center" className={classes.position}>
            <Grid
              direction="row"
              wrap="wrap"
              className={classes.verticalSpace}
              container
              spacing={1}
            >
              <Grid direction="row" wrap="wrap" item justify="center" xs="3">
                <Paper elevation={100} className={classes.paper}>
                  {<UserPerfil />}
                  <br />
                  <br />
                  {<ReadingBooks />}
                </Paper>
              </Grid>
              <Grid direction="row" wrap="wrap" item justify="center" xs="6">
                <Paper elevation={100} className={classes.paper}>
                  {<ReadBooks />}
                </Paper>
              </Grid>
              <Grid direction="row" wrap="wrap" item justify="center" xs="3">
                <Paper elevation={100} className={classes.paper}>
                  {<WishCard />}
                </Paper>
              </Grid>
            </Grid>
          </Container>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

const mapDispatchToProps = (dispatch) => ({
  addBooks: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(useStyles)(Profile)));

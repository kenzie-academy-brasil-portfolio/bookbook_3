import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Container } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import UserEdit from "./UserEdit";
const useStyles = (theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(0.1),
      width: "15ch",
      color: "white",
    },
  },
  superior: {
    width: "100%",
    height: "4px",
    backgroundColor: "rgb(23,19,11)",
    borderRadius: "5px",
  },
  h3: {
    margin: "0 auto",
    background:
      "linear-gradient(180deg, rgba(66,54,32,1) 0%, rgba(0,0,0,1) 100%)",
    borderRadius: "4px",
    color: "white",
    width: "100%",
    fontFamily: "Monda",
    fontSize: "18px",
  },
  userprofile: {
    margin: "0 auto",
    width: "100%",
    minWidth: "290px",
    maxWidth: "290px",
  },
  name: {
    color: "black",
    fontFamily: "Monda",
    fontSize: "18px",
  },
  photo: {
    width: "170px",
    height: "170px",
    borderRadius: "50%",
  },
  favoriteBook: {
    fontFamily: "Monda",
    fontSize: "14px",
  },
  button2: {
    backgroundColor: "white",
    color: "crimson",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "indianred",
  },
  dot: {
    color: "silver",
  },
});

class UserPerfil extends React.Component {
  state = { editIt: null };

  setEditing = () => {
    this.setState({ editIt: true });
  };
  cancelEditing = () => {
    this.setState({ editIt: false });
  };
  render() {
    const { user } = this.props;
    const { classes } = this.props;
    return (
      <div className={classes.userprofile}>
        <div className="userPerfil">
          <h1 className={classes.h3}>{user.user}</h1>
          <Paper elevation={5} className="UserPaper">
            <div className={classes.superior}></div>
            <br />
            <img
              src={user.image_url}
              alt="user"
              className={classes.photo}
            ></img>
            <h2 className={classes.name}>
              {user.name}
              {user.lastname}
            </h2>
            <Container className={classes.favoriteBook}>
              <p className={classes.dot}>●</p>
              <Container>{user.about}</Container>
            </Container>
            <Container>
              <UserEdit
                isEditing={this.state.editIt}
                cancelEditing={this.cancelEditing}
              ></UserEdit>
            </Container>
            <Container className={classes.favoriteBook}>
              <p className={classes.dot}>●</p>
              {user.address}
              <br />
              <Button
                className={classes.button2}
                size="small"
                variant="outlined"
                color="secondary"
                onClick={this.setEditing}
              >
                Editar
              </Button>
              <br />
            </Container>
          </Paper>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.session.user,
});
export default connect(mapStateToProps)(
  withRouter(withStyles(useStyles)(UserPerfil))
);

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper, CardMedia, Container } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import { connect } from "react-redux";

const styles = () => ({
  background: {
    backgroundColor: "GhostWhite	",
  },
  item: {
    margin: "0 auto",
    width: "100%",
    minWidth: "390px",
    maxWidth: "390px",
    minHeight: "500px",
  },
  superior: {
    width: "100%",
    height: "4px",
    backgroundColor: "rgba(18,29,77,1)",
    borderRadius: "5px",
  },
  name: {
    color: "#3f51b5",
    fontFamily: "Monda",
    textTransform: "uppercase",
    fontSize: "16px",
    marginTop: "-12px",
    marginBottom: "0px",
  },
  info: {
    color: "#374242",
    fontFamily: "Monda",
    fontSize: "15px",
    marginTop: "0px",
    marginBottom: "10px",
    textAlign: "center",
  },
  cards: {
    marginLeft: "8px",
    marginRight: "8px",
  },
  title: {
    textTransform: "uppercase",
    fontSize: "12px",
  },
  h2: {
    color: "#374242",
    fontFamily: "Monda",
    fontSize: "12px",
  },
  bookbook: {
    backgroundColor: "black",
    width: "100%",
    margin: "0 auto",
    color: "white",
    fontFamily: "Monda",
    fontWeight: "900",
    fontSize: "32px",
    textAlign: "center",

    marginBottom: "100px",
  },
  media: {
    margin: "0 auto",
    marginBottom: "10px",
  },
  button: {
    backgroundColor: "#antiquewhite",
    color: "darkslateblue",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "darkslateblue",
  },
  button2: {
    backgroundColor: "white",
    color: "crimson",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "indianred",
  },
  header1: {
    color: "#406868",
    fontFamily: "Monda",
    fontSize: "16px",
  },
  buttonRoot: {
    flexGrow: 1,
  },
  position: {
    width: "1150px",
    marginTop: "54px",
    fontFamily: "Monda",
    fontSize: "16px",
  },
  line: {
    color: "gray",
    fontSize: "10px",
    textAlign: "center",
  },
  review: {
    textAlign: "center",
    fontSize: "14px",
  },
  author: {
    textAlign: "right",
    fontSize: "12px",
  },
  bookbook2: {
    margin: "0 auto",
    height: "100px",
    width: "940px",
    backgroundColor: "black",
    position: "fixed",
    top: "385px",
    left: "-430px",
    fontSize: "32px",
    textAlign: "center",
    fontWeight: "900",
    color: "ghostwhite",
    fontFamily: "Monda",
    transform: "rotate(270deg)",
    lineHeight: "100px",
  },
  loading1: {
    width: "100%",
  },
  loading2: {
    width: "50%",
    marginLeft: "45.5%",
  },
  reviewMessagem: {
    position: "flex",
    alignItems: "center",
  },
});

class Explorer extends React.Component {
  state = { loading: true, reviewedBooks: [] };

  componentDidMount() {
    const currentToken = localStorage.getItem("currentToken");
    const reviewUrl = "https://ka-users-api.herokuapp.com/book_reviews";
    fetch(reviewUrl, {
      method: "GET",
      headers: { Authorization: this.props.currentToken },
    })
      .then((res) => res.json())
      .then((res) => {
        this.setState({ loading: false, reviewedBooks: res });
        console.log(this.state);
      });
  }

  render() {
    const { classes } = this.props;
    const { reviewedBooks } = this.state;
    if (this.state.loading === false) {
      return (
        <>
          <div div className={classes.background}>
            <div className={classes.bookbook}>Novidades</div>
            <div className={classes.bookbook2}>BookBook</div>
            <Container className={classes.position}>
              <Container justify="center" className={classes.position}>
                <Grid
                  direction="Row"
                  wrap="wrap"
                  className={classes.verticalSpace}
                  container="true"
                  alignContent="space-around"
                  alignItems="stretch"
                  spacing={10}
                >
                  <Grid
                    direction="Row"
                    wrap="wrap"
                    className={classes.verticalSpace}
                    container="true"
                    alignContent="space-around"
                    alignItems="stretch"
                    spacing={10}
                  >
                    {reviewedBooks.map((book, key) => (
                      <>
                        <Grid alignItems="stretch" className={classes.cards}>
                          <Paper
                            elevation={5}
                            className={classes.item}
                            key={key}
                          >
                            <Paper elevation={25}>
                              <div className={classes.superior}></div>
                              <br />
                              <Container>
                                <p className={classes.info}>
                                  <b>
                                    <font color="rgba(18,29,77,1)">
                                      {book.creator.user}
                                    </font>
                                  </b>{" "}
                                  leu <b>{book.title}</b> de{" "}
                                  <i>{book.author}</i>.
                                </p>
                              </Container>
                              <hr />
                              <Container>
                                <CardMedia
                                  className={classes.media}
                                  image={book.image_url}
                                  title={book.title}
                                  style={{ height: 225, width: 150 }}
                                />
                              </Container>
                              <hr />
                            </Paper>
                            <Container className={classes.reviewMessagem}>
                              <p className={classes.review}>
                                <i>"{book.review}"</i>
                              </p>
                              <p className={classes.author}>
                                - <b>{book.creator.name}</b>
                              </p>
                              <br />
                            </Container>
                          </Paper>
                          <br />
                          <br />
                        </Grid>
                      </>
                    ))}
                  </Grid>
                </Grid>
              </Container>
            </Container>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className={classes.bookbook}>Carregando</div>
          <div className={classes.bookbook2}>BookBook</div>
          <Container className={classes.loading1}>
            <Container className={classes.loading2}>
              <CircularProgress />
            </Container>
          </Container>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

export default connect(mapStateToProps)(
  withRouter(withStyles(styles)(Explorer))
);

import React from 'react';

class RateText extends React.Component {
  render() {
    if (this.props.grade === 1) {
      return <div>Você não gostou deste livro.</div>;
    } else if (this.props.grade === 2) {
      return <div>Você achou este livro ok.</div>;
    } else if (this.props.grade === 3) {
      return <div>Você gostou deste livro.</div>;
    } else if (this.props.grade === 4) {
      return <div>Você gostou muito deste livro.</div>;
    } else if (this.props.grade === 5) {
      return <div>Você amou este livro.</div>;
    } else {
      return <div>O que você achou deste livro?</div>;
    }
  }
}

export default RateText;

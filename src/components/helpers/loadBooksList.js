
const loadBooksList = (userId, token) => {
  return fetch(
    `https://ka-users-api.herokuapp.com/users/${userId}/books`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    }
  )
}

export default loadBooksList;


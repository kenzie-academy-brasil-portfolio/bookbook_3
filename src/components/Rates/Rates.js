import React from "react";
import IconButton from "@material-ui/core/IconButton";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import StarIcon from "@material-ui/icons/Star";
import { addBooks } from "../../action";
import { connect } from "react-redux";
import loadBooksList from "../helpers/loadBooksList";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  star: {
    color: "Goldenrod",
  },
});

class Rates extends React.Component {
  setNewGrade = (grade) => {
    fetch(
      `https://ka-users-api.herokuapp.com/users/${this.props.user.id}/books/${this.props.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.currentToken,
        },
        body: JSON.stringify({
          book: {
            grade: grade,
          },
        }),
      }
    ).then(() =>
      loadBooksList(this.props.user.id, this.props.currentToken)
        .then((res) => res.json())
        .then((res) => {
          this.props.addBooks(res);
        })
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <IconButton aria-label="star1" onClick={() => this.setNewGrade(1)}>
          {this.props.grade >= 1 ? (
            <StarIcon className={classes.star} />
          ) : (
            <StarBorderIcon className={classes.star} />
          )}
        </IconButton>
        <IconButton aria-label="star2" onClick={() => this.setNewGrade(2)}>
          {this.props.grade >= 2 ? (
            <StarIcon className={classes.star} />
          ) : (
            <StarBorderIcon className={classes.star} />
          )}
        </IconButton>
        <IconButton aria-label="star3" onClick={() => this.setNewGrade(3)}>
          {this.props.grade >= 3 ? (
            <StarIcon className={classes.star} />
          ) : (
            <StarBorderIcon className={classes.star} />
          )}
        </IconButton>
        <IconButton aria-label="star4" onClick={() => this.setNewGrade(4)}>
          {this.props.grade >= 4 ? (
            <StarIcon className={classes.star} />
          ) : (
            <StarBorderIcon className={classes.star} />
          )}
        </IconButton>
        <IconButton aria-label="star5" onClick={() => this.setNewGrade(5)}>
          {this.props.grade >= 5 ? (
            <StarIcon className={classes.star} />
          ) : (
            <StarBorderIcon className={classes.star} />
          )}
        </IconButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

const mapDispatchToProps = (dispatch) => ({
  addBooks: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Rates));

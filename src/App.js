import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { TextField } from "@material-ui/core";
import { Container } from "@material-ui/core";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import PersonIcon from "@material-ui/icons/Person";
import SearchIcon from "@material-ui/icons/Search";
import PublicIcon from "@material-ui/icons/Public";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import HomeIcon from "@material-ui/icons/Home";
import SimpleBottomNavigation from "./components/bars/bottomNavigation/bottomNavigation.jsx";
import SignUp from "./components/signUp/signUp.jsx";
import AboutUs from "./components/aboutUs/aboutUs.jsx";
import Profile from "./components/profile/profile.jsx";
import Explorer from "./components/Explorer/Explorer.jsx";
import Search from "./components/SearchList/Search";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { login, clearLogin } from "./action";
import "./appBar.css";
import "./App.css";

const useStyles = (theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(2),
      width: "25ch",
      color: "gray",
    },
  },
  barraSuperior: {
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    margin: "0 auto",
    width: "100%",
    position: "fixed",
  },
  loginButton: {
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    color: "ghostwhite",
    borderColor: "ghostwhite",
  },
  signUpButton: {
    backgroundColor: "AntiqueWhite",
    color: "#F29F05",
  },
  formButton: {
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    color: "ghostwhite",
    borderColor: "ghostwhite",
  },
  textInput: {
    color: "green",
    backgroundColor: "white",
  },
  icon: {
    color: "white",
    textDecoration: "none",
    marginLeft: "0.5%",
  },
  firstIcon: {
    color: "white",
    textDecoration: "none",
    marginLeft: "5%",
    marginRight: "0.5%",
  },
  lastIcon: {
    color: "white",
    textDecoration: "none",
    marginLeft: "0.5%",
    marginRight: "0%",
  },
  first: {
    color: "GhostWhite",
  },
  button2: {
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    color: "ghostwhite",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "ghostwhite",
  },
  position: {
    width: "1200px",
  },
});

const defaultState = {
  email: { value: "", error: "", helperText: "" },
  password: { value: "", error: "", helperText: "" },
  //authenticated: false,
  //currentToken: "",
};
class App extends React.Component {
  state = defaultState;

  componentDidMount() {
    if (window.localStorage.getItem("currentToken")) {
      this.props.login(
        localStorage.getItem("currentToken"),
        JSON.parse(localStorage.getItem("currentUser"))
      );
    }
  }

  /*=============================================================================================================
..............................................E-MAIL...........................................................
===============================================================================================================*/

  setValueEmail = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      email: { ...this.state.email, value: value },
    });
  };

  formatEmail = () => {
    const formatEmail = this.state.email.value;

    return this.setState({
      ...this.state,
      email: {
        ...this.state.email,
        value: formatEmail.toLowerCase(),
      },
    });
  };

  verificationEmail = () => {
    const validateEmail = this.state.email.value;

    if (validateEmail === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else {
      return "";
    }
  };
  /*=============================================================================================================
.....................................................SENHA.......................................................
===============================================================================================================*/

  setValuePassword = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      password: { ...this.state.password, value: value },
    });
  };

  verificationPassword = () => {
    const validatePassword = this.state.password.value;
    if (validatePassword === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
  .................................................SUBMETER LOGIN..................................................
  ===============================================================================================================*/

  handleClick = () => {
    fetch("https://ka-users-api.herokuapp.com/authenticate", {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify({
        user: this.state.email.value,
        password: this.state.password.value,
      }),
    })
      .then((response) => {
        if (response.status === 401) {
          this.setState({
            ...this.state,
            email: {
              ...this.state.email,
              error: "true",
              helperText: "E-mail não encontrado",
            },
            password: {
              ...this.state.password,
              error: "true",
              helperText: "Senha incorreta",
            },
          });
          throw Error;
        } else if (response.status >= 200 && response.status < 300) {
          return response.json();
        }
      })

      .then((response) => {
        this.props.login(response.auth_token, response.user);
        localStorage.setItem("currentToken", response.auth_token);
        localStorage.setItem("currentUser", JSON.stringify(response.user));
        this.props.history.push("/");
        //this.props.history.push('/explorer');
      });
  };

  submeter = (e) => {
    e.preventDefault();
    const emailError = this.verificationEmail();
    const senhaError = this.verificationPassword();

    this.setState({
      ...this.state,
      email: {
        value: this.state.email.value,
        error: emailError === "" ? "" : "true",
        helperText: emailError,
      },
      password: {
        value: this.state.password.value,
        error: senhaError === "" ? "" : "true",
        helperText: senhaError,
      },
    });

    if (emailError === "" && senhaError === "") {
      this.handleClick();
    }
  };

  /*=============================================================================================================
  ..............................................................................................................
.................................................RENDER.......................................................
.............................................................................................................
===========================================================================================================*/
  render() {
    const { classes } = this.props;

    if (!this.props.currentToken) {
      return (
        <>
          <AppBar
            className={classes.barraSuperior}
            position="static"
            color="inherit"
          >
            <Toolbar>
              <Link id="link" to="/">
                <IconButton edge="start" color="inherit" aria-label="menu">
                  <HomeIcon />
                </IconButton>
              </Link>
              <Link id="link" to="/developers">
                <IconButton edge="start" color="inherit" aria-label="menu">
                  <InfoIcon />
                </IconButton>
              </Link>
              <div id="login">
                <form className={classes.root}>
                  <TextField
                    className={classes.textInput}
                    size="small"
                    error={this.state.email.error}
                    label="Nome de usuário:"
                    onChange={this.setValueEmail}
                    onBlur={this.formatEmail}
                    value={this.state.email.value}
                    variant="filled"
                    fullWidth="true"
                    required="true"
                  />
                  <TextField
                    className={classes.textInput}
                    size="small"
                    error={this.state.password.error}
                    label="Senha:"
                    onChange={this.setValuePassword}
                    value={this.state.password.value}
                    variant="filled"
                    fullWidth="true"
                    type="password"
                    required="true"
                  />
                  <Button
                    className={classes.loginButton}
                    type="submit"
                    variant="outlined"
                    color="primary"
                    onClick={(e) => this.submeter(e)}
                  >
                    ENTRAR
                  </Button>
                </form>
              </div>
            </Toolbar>
          </AppBar>

          <Switch>
            <Route path="/developers">
              <AboutUs />
            </Route>
            <Route exact path="/">
              <SignUp />
            </Route>
          </Switch>
        </>
      );
    }
    if (this.props.currentToken) {
      return (
        <>
          <AppBar
            className={classes.barraSuperior}
            position="static"
            color="inherit"
          >
            <Container className={classes.first}>
              <Button
                className={classes.button2}
                color="secondary"
                size="small"
                variant="outlined"
                onClick={() => {
                  window.localStorage.clear();
                  this.props.clearLogin();
                  this.props.history.push("/");
                }}
              >
                Logout
              </Button>
              <Link to="/">
                <BottomNavigationAction
                  label="Explorar"
                  showLabel
                  className={classes.firstIcon}
                  icon={<PublicIcon />}
                />
              </Link>
              <Link to="/profile">
                <BottomNavigationAction
                  label="Perfil"
                  showLabel
                  className={classes.icon}
                  icon={<PersonIcon />}
                />
              </Link>
              <Link to="/search">
                <BottomNavigationAction
                  label="Buscar"
                  showLabel
                  className={classes.lastIcon}
                  icon={<SearchIcon />}
                />
              </Link>
            </Container>
          </AppBar>
          <SimpleBottomNavigation />
          <Switch>
            <Route path="/profile">
              <Profile />
            </Route>
            <Route path="/search">
              <Search />
            </Route>
            <Route exact path="/">
              <Explorer />
            </Route>
            <Route path="/search">
              <Search />
            </Route>
          </Switch>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});
const mapDispatchToProps = (dispatch) => ({
  login: (currentToken, user) => dispatch(login(currentToken, user)),
  clearLogin: () => dispatch(clearLogin()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(useStyles)(App)));
